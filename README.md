All the parameters will be adjusted in the code. The available options are:
* batch size: 32
* learning rate: 1e-3
* optimizer: Adam
* loss function: Cross Entropy Loss
* epoch: 100
* prune cycles: 20
* prune rate: %10 per cycle
* models: fc for LeNet-300-100, lenet5 for LeNet-5
* prune method: Random or Original Random reinitialization

Run command: *python3 project.py*